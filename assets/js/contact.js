const name = document.getElementById('name');
const email = document.getElementById('email');
const message = document.getElementById('msg');
//const phone = document.getElementById('phone')

name.addEventListener('blur', valName);
email.addEventListener('blur', valEmail);
message.addEventListener('blur', valMessage);
// phone.addEventListener('blur', valPhone);

function valName() {
  const re = /^[a-zA-Z ]{2,20}$/;
  if(!re.test(name.value)) {
    name.classList.add('is-invalid');
  }
  else {
    name.classList.remove('is-invalid');
  }
}

function valEmail() {
  const re = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
  if(!re.test(email.value)) {
    email.classList.add('is-invalid');
  }
  else {
    email.classList.remove('is-invalid');
  }
}

function valMessage() {
    if (message.value == "") {
        message.classList.add('is-invalid');
    }
    else {
        message.classList.remove('is-invalid');
    }
}