# Peace For Zion Website

This repository serves as the backend for [PeaceForZion.org](https://peaceforzion.org), a Gitlab Pages website.

## File Structure

    .
    ├── assets
    | ├── css
    | ├── fonts
    | ├── images
    | └── js
    ├── beliefs-about-israel
    | └── index.html
    ├── calendar
    | ├── index.html
    | └── indexRaw.html.gpg
    ├── common-accusations
    | └── index.html
    ├── contact-us
    | └── index.html
    ├── doctrinal-statement
    | └── index.html
    ├── objections
    | └── index.html
    ├── outreach-methodology
    | └── index.html
    ├── slogans
    | └── index.html
    ├── syllogism
    | └── index.html
    ├── what-we-do
    | └── index.html
    ├── index.html
    ├── .gitignore
    └── README.md

## Equip Backend
The `Equip` backend is managed by the [Equip Gitlab Repository](https://gitlab.com/peace-for-zion/resources.peaceforzion.org). As such, the `quartz` directory is ignored by Git for this project.